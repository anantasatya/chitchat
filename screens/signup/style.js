import { StyleSheet } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {normalize} from 'react-native-elements';

const styles = StyleSheet.create({
    container:{
        flex : 1,
        alignItems : 'center',
        justifyContent : 'center',
        backgroundColor : 'white',
    },
    header:{
        width: '100%',
        height : hp('20%'),
    },
    judul:{
        textAlign: 'left',
        fontFamily : 'RobotoBold',
        fontSize: normalize(26),
    },
    container_txtinput:{
        justifyContent: 'center',
        alignItems:'center',
        marginTop:hp('6.7%'),
    },
    txtInput:{
        borderBottomWidth : 1,
        borderBottomColor : '#C4C4C4',
        width : wp('80%'),
        paddingHorizontal: 15,
        marginBottom : hp('2.3%'),
        flexDirection:'row',
        justifyContent : 'space-between',
        alignItems : 'center',

    },
    txtInput_Konfirm_Err:{
        borderWidth : 1,
        borderColor : 'red',
        borderRadius: 15,
        width : wp('80%'),
        paddingHorizontal: 15,
        marginBottom : hp('2.3%'),
        flexDirection:'row',
        justifyContent : 'space-between',
        alignItems : 'center',

    },
    txtInput_Konfirm_Err2:{
        borderWidth : 1,
        borderColor : 'red',
        borderRadius: 15,
        width : wp('80%'),
        paddingHorizontal: 15,
        marginBottom : hp('2.3%'),
        flexDirection:'column',
        justifyContent : 'space-between',
        alignItems : 'center',

    },
    input:{
        fontFamily: 'RobotoRegular',
        paddingLeft: 5,
        paddingRight: 15,
        width: '90%',
    },
    button:{
        justifyContent : 'center',
        alignItems : 'center',
        width : wp('80%'),
        height:hp('7.03%'),
        borderRadius : 30,
        backgroundColor : '#e9c46a',
        marginTop: hp('2.9%'),
    },
    txt_btn:{
        fontFamily : 'RobotoMedium',
        fontSize : normalize(15),
        color : '#FFFF'
    },
    tanya:{
        flexDirection : "row",
        justifyContent : "space-between",
        alignItems : "center",
        marginTop : hp('4%'),
    },
    txt_tanya:{
        fontFamily : 'RobotoMedium',
        fontSize : normalize(14),
        letterSpacing : 1,
    },
    errorText:{
        alignSelf : "flex-start",
        marginTop: hp('-2%'),
        marginBottom : hp('1.5%'),
        fontSize : normalize(13),
        fontFamily : 'RobotoRegular',
        color : 'red',
    }
})

export default styles;