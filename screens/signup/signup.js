import React, { Component } from 'react';
import { View, Text, Image, TextInput, TouchableWithoutFeedback, Keyboard, KeyboardAvoidingView, Platform, TouchableOpacity, StatusBar} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from './style';
import Button from '../../Components/button_reg'
import Icon from 'react-native-vector-icons/Ionicons';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { CommonActions } from '@react-navigation/native';



const DismissKeyboard = ({children}) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      {children}
    </TouchableWithoutFeedback>
);

export default class Signup extends Component{
    constructor(props){
        super(props);
        this.state = {
            hidden : true,
            nama_lengkap : '',
            username : '',
            email : '',
            password : '',
            confirm_password : '',
            error : null,
            isLoading : false,
        }
    }

    toggleVisibility = () =>{
        this.setState({hidden: !this.state.hidden});
    }

 
    signUp = () => {
        const newUser = {
            nama : this.state.nama_lengkap,
            email : this.state.email,
            password : this.state.password,
        };
        this.setState({isLoading : true});
        //TODO: validate data
        let token, user, userId;
        auth()
            .createUserWithEmailAndPassword(newUser.email, newUser.password)
            .then( data => {
                userId = data.user.uid; 
                console.log('pass');
                return data.user.getIdToken();
                
            })
            .then(idToken => {
                    token = idToken;
                    user = auth().currentUser;
                    const userCredential = {
                        nama : newUser.nama,
                        email : newUser.email,
                        userId,
                        createdAt : new Date().toISOString(),
                    }
                    console.log('pass2');
                    return database().ref(`/users/${userId}`).set(userCredential);
                
            })
            .then( () => {
                    this.setState({isLoading : false});
            
                    this.props.navigation.dispatch(
                        CommonActions.reset({
                          index: 0,
                          routes: [{ name: 'App' }],
                        })
                      );
                
            })
            .catch(err => {
                if(err.code === "auth/email-already-in-use"){
                    this.setState({isLoading : false});
                    console.error(err);
                    return this.setState({ error: "Email sudah ada yang menggunakan!"})
                    
                }else {
                    this.setState({isLoading : false});
                    console.error(err);
                    return this.setState({ error: 'Something went wrong!'});
                }
                
            });
        }
    render(){
        return(
            <DismissKeyboard>
                    <View style={styles.container} >
                    <ActivityIndicator
                            size="large" 
                            color="#e9c46a" 
                            animating={this.state.isLoading} />
                        <View style={{width: '80%'}}>
                            <Text style={styles.judul}>SignUp, it's free!</Text>
                        </View>
                        <View style={styles.container_txtinput}>
                            <View style={styles.txtInput}>
                                <TextInput 
                                    placeholder='Nama Lengkap' 
                                    keyboardType='default' 
                                    style={styles.input}
                                    onChangeText={text => this.setState({nama_lengkap: text})}/>
                            </View>
                            <View style={this.state.error === "Email sudah ada yang menggunakan!" ? styles.txtInput_Konfirm_Err2 : styles.txtInput}>
                                <TextInput 
                                    placeholder='Alamat Email' 
                                    keyboardType='email-address' 
                                    style={styles.input}
                                    onChangeText={text => this.setState({email: text})}
                                    />
                                    
                            </View>
                            {
                                this.state.error === "Email sudah ada yang menggunakan!" ? <Text style={styles.errorText}>Email sudah ada yang menggunakan!</Text> : null
                            }

                            <View style={styles.txtInput}>
                                <TextInput 
                                    placeholder='Password' 
                                    keyboardType='default' style={styles.input} 
                                    secureTextEntry={this.state.hidden ? true : false }
                                    onChangeText={text => this.setState({password: text})}
                                    />
                                <TouchableOpacity onPress={this.toggleVisibility}>
                                    <Icon name={this.state.hidden ? "md-eye-off" : "md-eye"} size={26} color='#e9c46a'/>
                                </TouchableOpacity>
                            </View>
                            {
                                this.state.password.length < 8 && this.state.password !== '' ? <Text style={styles.errorText}>Password harus memiliki panjang minimal 8 karakter!</Text> : null
                            }
                            
                            
                            
                        </View>
                        <TouchableOpacity onPress={this.signUp} style={{marginTop: hp('5%')}}>
                            <Button name='Daftar' style_button={styles.button} style_txtButton={styles.txt_btn}/>
                        </TouchableOpacity>
                        <View style={styles.tanya}>
                            <Text style={styles.txt_tanya}>Sudah punya akun?</Text>
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate('Login')}>
                                <Text style={[styles.txt_tanya, {color: '#ffa630', marginLeft: wp('1.5%')}]}>Masuk</Text>
                            </TouchableOpacity>
                        </View>
                      
                        
                    </View>
                    
               </DismissKeyboard>  
        );
    }
}