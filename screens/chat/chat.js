import React, { Component } from 'react'
import { FlatList, TextInput, View, ScrollView, TouchableWithoutFeedback, Keyboard } from 'react-native'
import CardForum from '../../Components/Card_Forum';
import { Button } from 'react-native-elements';
import styles from './style';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { PopUp2 } from '../../Components/popUp'

var BayesClassifier = require('bayes-classifier')
var classifier = new BayesClassifier()

var sportDocuments = [
    `Juventus membeli Cristinao Ronaldo dari Real Madrid dengan mahar 100 juta euro pada 2018`,
    `Ronaldo vs Messi, Siapa Pemilik Pesawat Jet Pribadi Terkeren?`,
    `Sepak bola menjadi olahraga favorit dunia`,
    `Piala dunia 2022, siapa yang menang?`,
    'Siapa pemain sepak bola dengan bayaran termahal di dunia ?',
    'Legenda bulu tangkis Indonesia itu siapa ?'
  ]
  
  var educationDocuments = [
    `Pelajaran olahraga yang ditunggu-tunggu oleh siswa sekolah dasar adalah sepak bola`,
    `Bahasa Indonesia sebagai mata pelajaran yang menurut sebagian besar siswa sulit`,
    `Mata pelajaran programming harus mulai merambah ke sekolah`,
    `Sejarah merupakan pelajaran yang diminati`,
    'Sistem persenjataan kerajaan Majapahit apakah terhebat di nusantara pada waktu itu ?',
    'Apakah Ujian Nasional dihapus ?'
  ]

  var gamesDocuments = [
    `Mobile Legends Rajai E-sport Asia Tenggara di 2020`,
    `Garena Hadirkan Konten One-Punch Man di Free Fire`,
    `Hoodwink Diperkenalkan Valve Sebagai Hero Terbaru Dota 2`,
    `Apakah The Last of Us Part II jadi Game of the Year 2020 ?`,
    'GTA San Andreas, merupakan game open world',
    'Pemain e-sport terbaik dalam permainan PUBG itu siapa ?'
  ]

  var programmingDocuments = [
    `Bagaimana cara deklarasi variable di bahasa pemrograman javascript ?`,
    `Ada dua jenis component di React Native yaitu Function Component dan Class Component`,
    `Kegunaan constructor dalam setiap bahasa pemrograman ?`,
    `Bahasa pemrograman terbaik untuk android development`,
    'Java, Javascript, Dart, C++, C, C#, Phyton merupakan beberapa contoh bahasa pemrograman',
    'Cara mendeklarasikan function atau method dalam berbagai bahasa pemrograman',
    'Instansiasi objek pada bahasa pemrograman Java'
  ]


export class Sport extends Component{
    constructor(props){
        super(props);
        this.state = {
            data : [],
            nama: '',
            message: '',
            popUp : false,
        }
    }

    componentDidMount(){
        this.getProfil();
        this.getForum();
        classifier.addDocuments(sportDocuments, `sport`)
        classifier.addDocuments(educationDocuments, `education`)
        classifier.addDocuments(programmingDocuments, `programming`)
        classifier.addDocuments(gamesDocuments, `games`)

        classifier.train()

        
    }

    getProfil = () => {
        let uid = auth().currentUser.uid

        database().ref(`users/${uid}/nama`).once('value')
            .then( data => {
                console.log('datanya', data.val())
                this.setState({ nama : data.val()})
            })
            .catch(err => {
                console.log(err);
                
            })
    }

    post = () => {
        const posting = {
            nama : this.state.nama,
            message : this.state.message,
            createdAt : new Date().getTime(),
        }

        let check = classifier.classify(posting.message);
        if(check === 'sport'){
            database().ref(`chat/sport`).push(posting)
            .then(() => {
                console.log('berhasil mengupload data!')
                this.setState({ message: ''})
            })
            .catch(err => {
                console.log(err);
                
            })
        } else {
            this.setState({ popUp : true })
        }
    }

    accept = () => {
        const posting = {
            nama : this.state.nama,
            message : this.state.message,
            createdAt : new Date().getTime(),
        }

        let check = classifier.classify(posting.message);
        console.log('ini category', check)

        database().ref(`chat/${check}`).push(posting)
        .then(() => {
            console.log('berhasil mengupload data!')
            this.setState({ popUp : false, message: '' })
        })
        .catch(err => {
            console.log(err);
            
        })
        
    }
    togglePopup = () => {
        this.setState({popUp : !this.state.popUp})
    }

    getForum = () => {
        database().ref(`chat/sport/`).orderByChild('createdAt').on('value', chat => {
            console.log('chat', chat.val())
            
            let item = [];
            chat.forEach( data => {
                let subItem = {}
                subItem.id = data.key;
                subItem.data = data.val();

                
                let date = new Date(data.child('createdAt').val());
                console.log('date', date)
                var lastIndex = date.toString().lastIndexOf("GMT");
                let datefix = date.toString().substring(0, lastIndex);
                subItem.date = datefix;
                item.push(subItem)
                    
            })

            this.setState({ data : item})
            console.log('state',this.state.data)
        })
    }


    render(){
        console.log(this.state.data)
        return(
            <View style={{height: '65%'}}>
                <ScrollView>
                    <View style={styles.containerChat}>
                        <FlatList
                        data = {this.state.data}
                        renderItem = { ( {item} ) => {
                            return(
                                <CardForum
                                    name={item.data.nama}
                                    date={item.date}
                                    message={item.data.message}/>
                                
                            );
                        }}
                        contentContainerStyle = {{paddingBottom: 100, paddingHorizontal: 20, paddingTop: 10}}
                        keyExtractor={(item) => item.id}
                        />

                        

                        <PopUp2
                            isModalVisible = {this.state.popUp} 
                            text='Isi postingan tidak sesuai dengan topik forum! Apakah anda ingin menguploadnya sesuai dengan topik forum secara otomatis ?'
                           
                            accept = {this.accept}
                            decline= {this.togglePopup}/>
                    </View>
                </ScrollView>
                <View style={styles.addBtn}>
                    <TextInput style={styles.txtInput} placeholder='Isi postingan' value={this.state.message} onChangeText={text => this.setState({ message: text })}/>
                    
                    <Button
                        title="Post"
                        buttonStyle={
                            styles.btnPost
                        }
                        onPress={this.post}
                    />
                </View>
            </View>
            
        );
    }
}

export class Education extends Component{
    constructor(props){
        super(props);
        this.state = {
            data : [],
            popUp : false,
            message: '',
            nama: '',
        }
    }

    componentDidMount(){
        this.getProfil();
        this.getForum();
        classifier.addDocuments(sportDocuments, `sport`)
        classifier.addDocuments(educationDocuments, `education`)
        classifier.addDocuments(programmingDocuments, `programming`)
        classifier.addDocuments(gamesDocuments, `games`)

        classifier.train()

    
        
    }
    togglePopup = () => {
        this.setState({popUp : !this.state.popUp})
    }

    getProfil = () => {
        let uid = auth().currentUser.uid

        database().ref(`users/${uid}/nama`).once('value')
            .then( data => {
                console.log('datanya', data.val())
                this.setState({ nama : data.val()})
            })
            .catch(err => {
                console.log(err);
                
            })
    }

    post = () => {
        const posting = {
            nama : this.state.nama,
            message : this.state.message,
            createdAt : new Date().getTime(),
        }

        let check = classifier.classify(posting.message);
        if(check === 'education'){
            database().ref(`chat/education`).push(posting)
            .then(() => {
                console.log('berhasil mengupload data!')
                this.setState({ message: ''})
            })
            .catch(err => {
                console.log(err);
                
            })
        } else {
            this.setState({ popUp : true })
        }
    }

    accept = () => {
        const posting = {
            nama : this.state.nama,
            message : this.state.message,
            createdAt : new Date().getTime(),
        }

        let check = classifier.classify(posting.message);
        console.log('ini category', check)

        database().ref(`chat/${check}`).push(posting)
        .then(() => {
            console.log('berhasil mengupload data!')
            this.setState({ popUp : false, message: '' })
        })
        .catch(err => {
            console.log(err);
            
        })
        
    }

    getForum = () => {
        database().ref(`chat/education/`).orderByChild('createdAt').on('value', chat => {
            console.log('chat', chat.val())
            
            let item = [];
            chat.forEach( data => {
                let subItem = {}
                subItem.id = data.key;
                subItem.data = data.val();

                
                let date = new Date(data.child('createdAt').val());
                console.log('date', date)
                var lastIndex = date.toString().lastIndexOf("GMT");
                let datefix = date.toString().substring(0, lastIndex);
                subItem.date = datefix;
                item.push(subItem)
                    
            })

            this.setState({ data : item})
            console.log('state',this.state.data)
        })
    }

    render(){
        return(
            <View style={{height: '65%'}}>
                <ScrollView>
                    <View style={styles.containerChat}>
                        <FlatList
                        data = {this.state.data}
                        renderItem = { ( {item} ) => {
                            return(
                                <CardForum
                                    name={item.data.nama}
                                    date={item.date}
                                    message={item.data.message}/>
                                
                            );
                        }}
                        contentContainerStyle = {{paddingBottom: 100, paddingHorizontal: 20, paddingTop: 10}}
                        keyExtractor={(item) => item.id}
                        />

                    

                        <PopUp2
                            isModalVisible = {this.state.popUp} 
                            text='Isi postingan tidak sesuai dengan topik forum! Apakah anda ingin menguploadnya sesuai dengan topik forum secara otomatis ?'
                        
                            accept = {this.accept}
                            decline= {this.togglePopup}/>
                    </View>
                </ScrollView>
                <View style={styles.addBtn}>
                    <TextInput style={styles.txtInput} placeholder='Isi postingan' onChangeText={text => this.setState({ message: text })} value={this.state.message}/>
                    
                    <Button
                        title="Post"
                        buttonStyle={
                            styles.btnPost
                        }
                        onPress={this.post}
                    />
                </View>
            </View>
            
        );
    }
}

export class Programming extends Component{
    constructor(props){
        super(props);
        this.state = {
            data : [],
            message: '',
            popUp : false,
            nama: '',
        }
    }

    componentDidMount(){
        this.getProfil();
        this.getForum();
        classifier.addDocuments(sportDocuments, `sport`)
        classifier.addDocuments(educationDocuments, `education`)
        classifier.addDocuments(programmingDocuments, `programming`)
        classifier.addDocuments(gamesDocuments, `games`)

        classifier.train()

        
        
    }

    getProfil = () => {
        let uid = auth().currentUser.uid

        database().ref(`users/${uid}/nama`).once('value')
            .then( data => {
                console.log('datanya', data.val())
                this.setState({ nama : data.val()})
            })
            .catch(err => {
                console.log(err);
                
            })
    }

    post = () => {
        const posting = {
            nama : this.state.nama,
            message : this.state.message,
            createdAt : new Date().getTime(),
        }

        let check = classifier.classify(posting.message);
        if(check === 'programming'){
            database().ref(`chat/programming`).push(posting)
            .then(() => {
                console.log('berhasil mengupload data!')
                this.setState({ message: ''})
            })
            .catch(err => {
                console.log(err);
                
            })
        } else {
            this.setState({ popUp : true })
        }   
    }
    togglePopup = () => {
        this.setState({popUp : !this.state.popUp})
    }

    accept = () => {
        const posting = {
            nama : this.state.nama,
            message : this.state.message,
            createdAt : new Date().getTime(),
        }

        let check = classifier.classify(posting.message);
        console.log('ini category', check)

        database().ref(`chat/${check}`).push(posting)
        .then(() => {
            console.log('berhasil mengupload data!')
            this.setState({ popUp : false, message: '' })
        })
        .catch(err => {
            console.log(err);
            
        })
        
    }

    getForum = () => {
        database().ref(`chat/programming/`).orderByChild('createdAt').on('value', chat => {
            console.log('chat', chat.val())
            
            let item = [];
            chat.forEach( data => {
                let subItem = {}
                subItem.id = data.key;
                subItem.data = data.val();

                
                let date = new Date(data.child('createdAt').val());
                console.log('date', date)
                var lastIndex = date.toString().lastIndexOf("GMT");
                let datefix = date.toString().substring(0, lastIndex);
                subItem.date = datefix;
                item.push(subItem)
                    
            })

            this.setState({ data : item})
            console.log('state',this.state.data)
        })
    }

    render(){
        return(
            <View style={{height: '65%'}}>
                <ScrollView>
                    <View style={styles.containerChat}>
                        <FlatList
                        data = {this.state.data}
                        renderItem = { ( {item} ) => {
                            return(
                                <CardForum
                                    name={item.data.nama}
                                    date={item.date}
                                    message={item.data.message}/>
                                
                            );
                        }}
                        contentContainerStyle = {{paddingBottom: 100, paddingHorizontal: 20, paddingTop: 10}}
                        keyExtractor={(item) => item.id}
                        />

                        

                        <PopUp2
                            isModalVisible = {this.state.popUp} 
                            text='Isi postingan tidak sesuai dengan topik forum! Apakah anda ingin menguploadnya sesuai dengan topik forum secara otomatis ?'
                          
                            
                            accept = {this.accept}
                            decline= {this.togglePopup}/>
                    </View>
            </ScrollView>
            <View style={styles.addBtn}>
                <TextInput style={styles.txtInput} placeholder='Isi postingan' onChangeText={text => this.setState({ message: text })} value={this.state.message}/>
                
                <Button
                    title="Post"
                    buttonStyle={
                        styles.btnPost
                    }
                    onPress={this.post}
                />
            </View>
            </View>
            
            
            
        );
    }
}

export class Games extends Component{
    constructor(props){
        super(props);
        this.state = {
            data : [],
            popUp : false,
            message: '',
            nama: '',
        }
    }

    componentDidMount(){
        this.getProfil();
        this.getForum();
        classifier.addDocuments(sportDocuments, `sport`)
        classifier.addDocuments(educationDocuments, `education`)
        classifier.addDocuments(programmingDocuments, `programming`)
        classifier.addDocuments(gamesDocuments, `games`)

        classifier.train()

        
    }

    getProfil = () => {
        let uid = auth().currentUser.uid

        database().ref(`users/${uid}/nama`).once('value')
            .then( data => {
                console.log('datanya', data.val())
                this.setState({ nama : data.val()})
            })
            .catch(err => {
                console.log(err);
                
            })
    }

    post = () => {
        const posting = {
            nama : this.state.nama,
            message : this.state.message,
            createdAt : new Date().getTime(),
        }

        let check = classifier.classify(posting.message);
        console.log('ini category', check)
        if(check === 'games'){
            database().ref(`chat/games`).push(posting)
            .then(() => {
                console.log('berhasil mengupload data!')
                this.setState({ message: '' });
            })
            .catch(err => {
                console.log(err);
                
            })
        } else {
            this.setState({ popUp : true })
        }   
    }

    accept = () => {
        const posting = {
            nama : this.state.nama,
            message : this.state.message,
            createdAt : new Date().getTime(),
        }

        let check = classifier.classify(posting.message);
        console.log('ini category', check)

        database().ref(`chat/${check}`).push(posting)
        .then(() => {
            console.log('berhasil mengupload data!')
            this.setState({ popUp : false, message: ''})
        })
        .catch(err => {
            console.log(err);
            
        })
        
    }

    togglePopup = () => {
        this.setState({popUp : !this.state.popUp})
    }

    getForum = () => {
        database().ref(`chat/games/`).orderByChild('createdAt').on('value', chat => {
            console.log('chat', chat.val())
            
            let item = [];
            chat.forEach( data => {
                let subItem = {}
                subItem.id = data.key;
                subItem.data = data.val();

                
                let date = new Date(data.child('createdAt').val());
                console.log('date', date)
                var lastIndex = date.toString().lastIndexOf("GMT");
                let datefix = date.toString().substring(0, lastIndex);
                subItem.date = datefix;
                item.push(subItem)
                    
            })

            this.setState({ data : item})
            console.log('state',this.state.data)
        })
    }

    render(){
        return(
            <View style={{height: '65%'}}>
                <ScrollView >
                    <View style={styles.containerChat}>
                        <FlatList
                        data = {this.state.data}
                        renderItem = { ( {item} ) => {
                            return(
                                <CardForum
                                    name={item.data.nama}
                                    date={item.date}
                                    message={item.data.message}/>
                                
                            );
                        }}
                        contentContainerStyle = {{paddingBottom: 100, paddingHorizontal: 20, paddingTop: 10}}
                        keyExtractor={(item) => item.id}
                        />

                    

                        <PopUp2
                            isModalVisible = {this.state.popUp} 
                            text='Isi postingan tidak sesuai dengan topik forum! Apakah anda ingin menguploadnya sesuai dengan topik forum secara otomatis ?'
                            
                            accept = {this.accept}
                            decline= {this.togglePopup}/>
                    </View>
                </ScrollView>
                <View style={styles.addBtn}>
                    <TextInput style={styles.txtInput} placeholder='Isi postingan' onChangeText={text => this.setState({ message: text })} value={this.state.message}/>
                    
                    <Button
                        title="Post"
                        buttonStyle={
                            styles.btnPost
                        }
                        onPress={this.post}
                    />
                </View>
            </View>
            
            
            
        );
    }
}