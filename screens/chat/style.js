import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    
    subContainer: {
        flexDirection : 'row',
        justifyContent : "space-around",
        alignItems : "center",
        paddingVertical : 20,
        
    },
    fragmentActive: {
        borderBottomWidth : 1,
        borderBottomColor : '#e76f51',
    },
    
    addBtn: {
        position: "absolute",
        right: 0,
        bottom: 0,
        left: 0,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        width: '100%',
        padding : 15,
        borderWidth: 1,
        borderColor: '#C4C4C4',
        backgroundColor: 'white',
    },
    containerChat: {
        height: '92%',
    },
    txtInput: {
        borderWidth: 1,
        borderRadius : 10,
        width: 250,
        height: 45,
        borderColor: '#C4C4C4'

    },
    btnPost: {
        width: 100,
        height: 45,
        backgroundColor: '#e9c46a'
        
    },
    container_logout: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems:'center',
        marginRight: 15,
        marginTop: 15,
    },
    logout:{
        marginRight: 10,
        fontSize: 16,
        color: 'white'
    },
    header:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
        paddingBottom: 15,
        width: '100%',
        backgroundColor: '#e9c46a'
    },
    txtHeader:{
        fontSize: 18,
        marginLeft: 15,
        marginTop: 15,
        color: 'white'
    }
})

export default styles;