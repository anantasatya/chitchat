import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Fragment from '../../Components/Fragment';
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons';
import { Sport, Education, Programming, Games } from './chat';
import auth from '@react-native-firebase/auth';
import { CommonActions } from '@react-navigation/native';


export default class Chat extends Component{
    constructor(props){
        super(props);
        this.state = {
            fragment1_Clicked : true,
            fragment2_Clicked : false,
            fragment3_Clicked : false,
            fragment4_Clicked : false,
            isFocused1 : true,
            isFocused2: false,
            isFocused3: false,
            isFocused4: false,
        };
    }

    logout = () => {
        auth()
            .signOut()
            .then(() => {
                this.props.navigation.dispatch(
                CommonActions.reset({
                  index: 0,
                  routes: [{ name: 'Login' }],
                })
              );
            });
    }

    showFragmentSport = () => {
        this.setState({fragment1_Clicked : true, fragment2_Clicked : false, fragment3_Clicked : false, fragment4_Clicked : false, isFocused1: true, isFocused2: false, isFocused3: false, isFocused4: false});
    }

    showFragmentEducation = () => {
        this.setState({fragment1_Clicked : false, fragment2_Clicked : true, fragment3_Clicked : false, fragment4_Clicked : false, isFocused1: false, isFocused2: true, isFocused3: false, isFocused4: false});
    }

    showFragmentProgramming = () => {
        this.setState({fragment1_Clicked : false, fragment2_Clicked : false, fragment3_Clicked : true, fragment4_Clicked : false, isFocused1: false, isFocused2: false, isFocused3: true, isFocused4: false});
    }

    showFragmentGames = () => {
        this.setState({fragment1_Clicked : false, fragment2_Clicked : false, fragment3_Clicked : false, fragment4_Clicked : true, isFocused1: false, isFocused2: false, isFocused3: false, isFocused4: true});
    }
    

    renderElement() {
        if (this.state.fragment1_Clicked === true && this.state.fragment2_Clicked === false && this.state.fragment3_Clicked === false && this.state.fragment4_Clicked === false) {
            return <Sport/>
        } else if (this.state.fragment1_Clicked === false && this.state.fragment2_Clicked === true && this.state.fragment3_Clicked === false && this.state.fragment4_Clicked === false) {
            return <Education/>
        } else if (this.state.fragment1_Clicked === false && this.state.fragment2_Clicked === false && this.state.fragment3_Clicked === true && this.state.fragment4_Clicked === false) {
            return <Programming/>
        }else if (this.state.fragment1_Clicked === false && this.state.fragment2_Clicked === false && this.state.fragment3_Clicked === false && this.state.fragment4_Clicked === true) {
            return <Games/>
        }
    }
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.txtHeader}>ChitChat</Text>
                    <TouchableOpacity style={styles.container_logout} onPress={this.logout}>
                        <Text style={styles.logout}>Logout</Text>
                        <Icon name='md-log-out' color="#264653" size={30}/>
                    </TouchableOpacity>
                </View>
                
                
                <View style={styles.subContainer}>
                    <TouchableOpacity onPress={this.showFragmentSport} >
                        <Fragment
                            name='Sports'
                            style={this.state.isFocused1 === true ? styles.fragmentActive: null}
                            style_txt={{marginVertical: 10, textAlign : 'center', fontSize: 14}}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showFragmentEducation} >
                        <Fragment
                            name='Education'
                            style_txt={{marginVertical: 10, textAlign : 'center', fontSize: 14}}
                            style={this.state.isFocused2 === true ? styles.fragmentActive: null}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showFragmentProgramming}>
                        <Fragment
                            name='Programming'
                            style_txt={{marginVertical: 10, textAlign : 'center', fontSize: 14}}
                            style={this.state.isFocused3 === true ? styles.fragmentActive: null}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showFragmentGames}>
                        <Fragment
                            name='Games'
                            style_txt={{marginVertical: 10, textAlign : 'center', fontSize: 14}}
                            style={this.state.isFocused4 === true ? styles.fragmentActive: null}/>
                    </TouchableOpacity>
                </View>
                <View style={{height: '100%'}}>
                    {this.renderElement()}
                </View>
                
            </View>
        
        );
    }
}