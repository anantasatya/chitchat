import React, { Component, useRef } from 'react';
import { View, Text, Image, TextInput, TouchableWithoutFeedback, Keyboard, ActivityIndicator, ToastAndroid, TouchableOpacity, StatusBar} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import styles from './style';
import Button from '../../Components/button_reg'
import Icon from 'react-native-vector-icons/Ionicons';
import auth from '@react-native-firebase/auth';
import Popup, { LupaPass } from '../../Components/popUp';
import { CommonActions } from '@react-navigation/native';



const DismissKeyboard = ({children}) => (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          {children}
        </TouchableWithoutFeedback>
);


export default class Login extends Component{
    constructor(props){
        super(props);
        this.state = {
            hidden : true,
            error : '',
            isVisible : false,
            isVisibleLupaPass : false,
            email : '',
            password : '',
            hide : true,
            inputFocused : null,
            isLoading : false,
            
        }
    }

    toggleVisibility = () =>{
        this.setState({hidden: !this.state.hidden});
    }

    toggleLupaPass = () => {
        this.setState({isVisibleLupaPass: !this.state.isVisibleLupaPass});
    }


    togglePopup = () => {
        this.setState({isVisible : !this.state.isVisible})
    }

    resetPass = () => {
        auth().sendPasswordResetEmail(this.state.email)
            .then(() => {
                this.setState({ hide : true });
                console.log('email dikirim!');
                this.setState({ isVisibleLupaPass : false });
                ToastAndroid.show("Email reset password berhasil dikirim!", ToastAndroid.LONG);
          }).catch(error => {
            console.log(error);
            if(error.code === 'auth/user-not-found'){
                this.setState({ hide : false });
                return this.setState({ error : 'Email yang Anda inputkan tidak terdaftar di sistem kami!'});
            }else {
                console.log(error)
                return this.setState({ error : 'Maaf ada kesalahan! Silahkan coba lagi.', hide: false});
            }
            
          });
    }


    login = () => {
        const user = {
            email : this.state.email,
            password : this.state.password
        };
        this.setState({ isLoading : true });
        let verified, userData;
        auth().signInWithEmailAndPassword(user.email, user.password)
            .then( async(token) => {
                userData = auth().currentUser;
                await userData.reload();
                userData = auth().currentUser;

            })
            .then(data => {
                
                    let tokenUser = data;
                    console.warn('data', tokenUser);
                    this.setState({ isLoading : false });
                    return this.props.navigation.dispatch(
                        CommonActions.reset({
                          index: 0,
                          routes: [{ name: 'App' }],
                        })
                      );
                
                
            })
            .catch(err => {
                console.error(err);
                if(err.code === 'auth/wrong-password' || err.code === 'auth/user-not-found'){
                    this.setState({ isVisible : true, isLoading : false });

                    return this.setState({ error: "Email/Password yang dimasukkan salah!"})
                } else if(err.code === 'auth/invalid-email'){
                    this.setState({ isLoading : false });

                    return this.setState({ error: "Format email tidak valid!"})
                }
                this.setState({ isVisible : true, isLoading : false });
                return this.setState({ error : 'Terdapat masalah! Silahkan coba lagi.' });
            });
    }
    

    render(){
        console.log('hide', this.state.hide)
        return(
                <DismissKeyboard>
                    <View style={styles.container} >
                    <StatusBar barStyle='light-content' backgroundColor='#e9c46a'/>
                    <ActivityIndicator
                            size="large" 
                            color="#e9c46a" 
                            animating={this.state.isLoading} />
                        {/* <Image source={require('../../assets/img/header-login.png')} style={styles.header} resizeMode='stretch'/> */}
                        <View style={{width: '80%'}}>
                            <Text style={styles.judul}>Login</Text>
                            
                        </View>
                        <View style={styles.container_txtinput}>
                            <View style={this.state.error === "Format email tidak valid!" ? styles.txtInput_Konfirm_Err2 : styles.txtInput}>
                                <TextInput 
                                    placeholder='Alamat Email' 
                                    keyboardType='email-address' 
                                    style={styles.input}
                                    onFocus={() => this.setState({ inputFocused : true })}
                                    onBlur={()=> this.setState({inputFocused: false})}
                                    onChangeText={text => this.setState({email: text})}/>
                            </View>
                            {
                                this.state.error === 'Format email tidak valid!' ? 
                                    <Text style={styles.errorText}>Format email tidak valid!</Text>
                                        :
                                    null
                            }
                            <View style={styles.txtInput2}>
                                <TextInput 
                                    placeholder='Password' 
                                    keyboardType='default' 
                                    style={styles.input} 
                                    secureTextEntry={this.state.hidden ? true : false }
                                    onChangeText={text => this.setState({password: text})}/>
                                <TouchableOpacity onPress={this.toggleVisibility}>
                                    <Icon name={this.state.hidden ? "md-eye-off" : "md-eye"} size={26} color='#e9c46a'/>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity style={{alignSelf : 'flex-end'}} onPress={this.toggleLupaPass}>
                                <Text style={styles.txtLupaPass}>Lupa password ?</Text>
                            </TouchableOpacity>
                            
                            
                            
                        </View>
                        <TouchableOpacity onPress={this.login} style={{marginTop: hp('7.9%')}}>
                            <Button name='Masuk' style_button={styles.button} style_txtButton={styles.txt_btn}/>
                        </TouchableOpacity>
                        <View style={styles.tanya}>
                            <Text style={styles.txt_tanya}>Belum punya akun?</Text>
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate('Signup')}>
                                <Text style={[styles.txt_tanya, {color: '#ffa630', marginLeft: wp('1.5%')}]}>Daftar</Text>
                            </TouchableOpacity>
                        </View>
                        <Popup 
                            isModalVisible = {this.state.isVisible} 
                            text={this.state.error}
                            onPress={this.togglePopup} 
                            nameIcon = 'account-alert-outline' 
                            colorIcon='red'/>
                        
                        <LupaPass 
                            isModalVisible = {this.state.isVisibleLupaPass} 
                            text='Masukkan alamat email anda! Kami akan mengirim email untuk reset password.'
                            closeModal={this.toggleLupaPass} 
                            nameIcon = 'lock-question' 
                            colorIcon='red'
                            onPress={this.toggleLupaPass}
                            kirim={this.resetPass}
                            onChangeText={text => {this.setState({email: text})}}
                            hide={this.state.hide}
                            message={this.state.error}/> 
                    </View>
               </DismissKeyboard>  
        );
    }
}