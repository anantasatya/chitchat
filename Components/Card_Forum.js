import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'

export default class CardForum extends Component{
    render(){
        return(
            <View style={styles.container_card}>
                <View style={styles.head}>
                    <Text>{this.props.name}</Text>
                    <Text>{this.props.date}</Text>
                </View>
                <Text>{this.props.message}</Text>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    head: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 10
    },
    container_card: {
        width: '100%',
        backgroundColor: 'white',
        borderRadius: 5,
        elevation: 7,
        padding : 15,
        marginBottom: 20
    }
})

