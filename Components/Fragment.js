import React, { Component } from 'react';
import { View, Text } from 'react-native';


export default class Fragment extends Component {
    render(){
        return(
            <View style={this.props.style}>
                <Text style={this.props.style_txt}>{this.props.name}</Text>
            </View>
        );
    }
}
