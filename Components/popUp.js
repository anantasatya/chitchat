import React, { Component } from 'react'
import { View, TouchableOpacity, Text, StyleSheet, TextInput } from 'react-native'
import { normalize } from 'react-native-elements';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Button from '../Components/button_reg';


export default class PopUp extends Component{
    constructor(props){
        super(props);
        this.state = {
            ongkir : '',
            popUp_ongkir :false,
        }

    }
    render(){
        return(
                <Modal 
                    isVisible={this.props.isModalVisible}
                    onBackdropPress={this.props.closeModal}
                    backdropColor="black"
                    backdropOpacity={0.8}
                    hasBackdrop={true}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={600}
                    animationOutTiming={600}
                    backdropTransitionInTiming={600}
                    backdropTransitionOutTiming={600}
                    >
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper}>
                            <TouchableOpacity onPress={this.props.onPress} style={{ alignSelf: 'flex-end'}}>
                                <Icon name='window-close' color='red' size={26} />
                            </TouchableOpacity>
                            {
                                this.props.text === 'Gagal mengupload menu!' ? 
                                    <Icon2 name={this.props.nameIcon} color={this.props.colorIcon} size={60}/> 
                                    :
                                    <Icon name={this.props.nameIcon} color={this.props.colorIcon} size={60}/> 
                            }
                            
                            <Text style={styles.text}>{this.props.text}</Text>
                        </View>
                    </View>
                </Modal>
        );
    }
}

export class PopUp2 extends Component{
    constructor(props){
        super(props);
        this.state = {
            ongkir : '',
            popUp_ongkir :false,
        }

    }
    render(){
        return(
                <Modal 
                    isVisible={this.props.isModalVisible}
                    onBackdropPress={this.props.closeModal}
                    backdropColor="black"
                    backdropOpacity={0.8}
                    hasBackdrop={true}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={600}
                    animationOutTiming={600}
                    backdropTransitionInTiming={600}
                    backdropTransitionOutTiming={600}
                    >
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper}>
                            
                            <Text style={styles.text}>{this.props.text}</Text>
                            <View>
                                <TouchableOpacity onPress={this.props.accept} style={{marginTop: hp('4%')}}>
                                    <Button name='Oke' style_button={styles.btn} style_txtButton={styles.txt_btn}/>
                                </TouchableOpacity> 
                                <TouchableOpacity onPress={this.props.decline}>
                                    <Button name='Batal' style_button={styles.btn_cancel} style_txtButton={styles.txt_btn_cancel}/>
                                </TouchableOpacity> 
                            </View>
                            

                        </View>
                    </View>
                </Modal>
        );
    }
}


export class LupaPass extends Component {
    render(){
        return(
            <Modal 
                    isVisible={this.props.isModalVisible}
                    onBackdropPress={this.props.closeModal}
                    backdropColor="black"
                    backdropOpacity={0.8}
                    hasBackdrop={true}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={600}
                    animationOutTiming={600}
                    backdropTransitionInTiming={600}
                    backdropTransitionOutTiming={600}
                    >
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper2}>
                            <TouchableOpacity onPress={this.props.onPress} style={{ alignSelf: 'flex-end'}}>
                                <Icon name='window-close' color='red' size={26} />
                            </TouchableOpacity>
                            
                            <Text style={styles.txtJudul}>Lupa Password</Text>
                            <Text style={styles.text}>{this.props.text}</Text>
                            <TextInput style={this.props.hide === false ? styles.txtInput_Konfirm_Err2 : styles.txtInput} keyboardType='email-address' placeholder='Email' onChangeText={this.props.onChangeText}/>
                            {
                                this.props.hide === false ? <Text style={styles.errorText}>{this.props.message}</Text> : null
                            }
                            <TouchableOpacity style={styles.btn} onPress={this.props.kirim}>
                                    <Text style={styles.txt_btn}>Kirim</Text>
                            </TouchableOpacity>
                            
                        </View>
                    </View>
                </Modal>
        )
    }
}



const styles = StyleSheet.create({
    modalBackground: {
        flex : 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        
    },
    activityIndicatorWrapper: {
      backgroundColor: '#FFFFFF',
      padding : 15,
      
      borderRadius: 10,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around'
    },
    activityIndicatorWrapper2: {
        backgroundColor: '#FFFFFF',
        padding : 15,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
      },
      activityIndicatorWrapper3: {
        backgroundColor: '#FFFFFF',
        padding : 15,
        height: hp('80%'),
        width: wp('89%'),
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
      },
    text: {
        fontFamily: 'RobotoRegular',
        fontSize: normalize(15),
        textAlign : "center",
        lineHeight: 25,
        marginTop: hp('5%')
    },
    text2: {
        fontFamily: 'RobotoRegular',
        fontSize: normalize(15),
        textAlign : "justify",
        lineHeight: 25,
    },
    txtInput:{
        borderWidth : 1,
        borderColor : '#C4C4C4',
        borderRadius: 15,
        width : wp('80%'),
        paddingHorizontal: 15,
        flexDirection:'row',
        justifyContent : 'space-between',
        alignItems : 'center',

    },
    btn_cancel:{
        justifyContent : 'center',
        alignItems : 'center',
        width : wp('50%'),
        height:hp('7.03%'),
        borderRadius : 30,
        marginTop: hp('3%'),
        backgroundColor : 'white',
    },
    btn:{
        justifyContent : 'center',
        alignItems : 'center',
        width : wp('50%'),
        height:hp('7.03%'),
        borderRadius : 30,
        marginTop: hp('3%'),
        backgroundColor : '#e9c46a',
    },
    
    txt_btn:{
        fontFamily : 'RobotoMedium',
        fontSize : normalize(15),
        color : '#FFFF'
    },
    txt_btn_cancel:{
        fontFamily : 'RobotoMedium',
        fontSize : normalize(15),
        color : 'red'
    },
    txtJudul: {
        fontFamily : 'RobotoBold',
        fontSize : normalize(24),
        lineHeight: 28,
        letterSpacing : 0.5
    },
    errorText:{
        alignSelf : "flex-start",
        marginTop: hp('-2%'),
        marginBottom : hp('2%'),
        marginLeft: wp('1%'),
        fontSize : normalize(13),
        fontFamily : 'RobotoRegular',
        color : 'red',
    },
    txtInput_Konfirm_Err2:{
        borderWidth : 1,
        borderColor : 'red',
        borderRadius: 15,
        width : wp('80%'),
        paddingHorizontal: 15,
        marginBottom : hp('2.3%'),
        flexDirection:'column',
        justifyContent : 'space-between',
        alignItems : 'center',

    },
    container_optionImage: {
        flexDirection : "row",
        justifyContent : "space-around",
        alignItems : "center",
        width : '100%'
    },
    container_confirm_ongkir: {
        flexDirection : "column",
        justifyContent : "center",
        alignItems : "center",
        width : '100%'
    },
    optionImage: {
        width : wp('30%'),
        height : hp('20%'),
        borderRadius : 10,
        borderWidth : 1,
        borderColor : '#C4C4C4',
        justifyContent : "center",
        alignItems : "center"
    },
    txtOption: {
        marginTop : hp('2%'),
        fontFamily : 'RobotoMedium',
        fontSize : normalize(15)
    },
    txtInputOngkir:{
        borderWidth : 1,
        borderColor : '#C4C4C4',
        width : wp('50.05%'),
        color : 'black',
        borderRadius : 15,
        alignSelf: 'flex-start',
        marginTop: hp('2%')
        
    },

    btn_upload :{
        backgroundColor:'#0077BE',
        width : wp('60,8%'),
        height : hp('7.09%'),
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 30,
        marginTop: hp('2%')
    },
    txt_btn:{
        fontFamily : 'Roboto-Regular',
        fontSize : normalize(15),
        color : '#FFFF'
    },
    box_img_upload:{
        borderWidth : 1,
        borderRadius : 10,
        borderColor : '#C4C4C4',
        height : 198,
        width : '100%',
    },
  });