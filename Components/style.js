import { StyleSheet } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {normalize} from 'react-native-elements'

const styles = StyleSheet.create({
    txtInput:{
        borderBottomWidth : 1,
        borderBottomColor : '#C4C4C4',
        width : '100%',
        paddingBottom : 5,
        fontSize: normalize(14),
        marginBottom : hp('3.5%'),
    },
    txtInputstok:{
        borderBottomWidth : 1,
        borderBottomColor : '#C4C4C4',
        width : '30%',
        paddingBottom : 5,
        fontSize: normalize(14),
        marginBottom : hp('3.5%'),
    },
    judul_txtinput:{
        fontFamily : 'Roboto-Medium',
        fontSize : normalize(16),
        
    },
    r_btn:{
        width : wp('5%'),
        height : wp('5%'),
        marginRight : wp('3%'),
    },
    item_rbtn:{
        fontSize : normalize(14),
        fontFamily : 'Roboto-Regular',
    },
    txt_item:{
        fontFamily : 'Roboto-Medium',
        fontSize : normalize(14),
    },
    jml:{
        width : 25,
        borderWidth : 1.5,
        color : '#0077BE',
        borderRadius : 5,
        marginLeft : wp('2%'),
        justifyContent : "center",
        alignItems : "center",
        padding : 2,
    },
    container_card:{
        width : '100%',
        borderRadius : 15,
        borderWidth : 0,
        marginBottom : 25,
        padding : 15,
        justifyContent : "space-between",
        elevation : 10,
        backgroundColor : '#FFFF'
        
    },
    txtJenisOrder:{
        textAlign : 'right',
        fontFamily : 'Roboto-Medium',
        fontSize : normalize(14),
        textTransform: 'capitalize',
        
    },
    container_subCard : {
        flexDirection : 'column',
        justifyContent : "space-between",
        alignItems : 'flex-end',
    },
    container_subCard2 : {
        flexDirection : 'column',
        justifyContent : "space-between",
        alignItems : 'flex-start',
    },
    txt_waktu_order:{
        fontFamily : 'Roboto-Italic',
        fontSize : normalize(14),
    },
    txt_namaCustomer:{
        fontFamily : 'Roboto-Regular',
        fontSize : normalize(14),
    },
    txt_no_order:{
        fontFamily : 'Roboto-Regular',
        fontSize : normalize(14),
    },
    box_img_upload:{
        borderWidth : 1,
        borderRadius : 10,
        borderColor : '#C4C4C4',
        height : 198,
        width : '100%',
        flexDirection : 'row',
        justifyContent : 'space-between',
        alignItems : 'flex-start',
        padding : 15,
    },
    container_card_menu:{
        borderWidth : 1,
        borderColor : '#C4C4C4',
        height : hp('47%'),
        width : '100%',
        borderRadius : 15,
        marginBottom : 35,
        borderWidth : 0,
        elevation : 10,
        backgroundColor : '#FFFF'
    },
    container_keterangan_cardMenu:{
        padding : 15,
        justifyContent : "space-between",
    },
    container_subketerangan_cardMenu:{
        flexDirection : 'row',
        justifyContent : 'space-between',
        
    },
    txt_keterangan_cardMenu:{
        marginBottom : 6,
    },
    harga :{
        fontFamily : 'Roboto-Regular',
        fontSize : normalize(14),
    },
    container_btn_akun:{
        width : '100%',
        height : hp('7%'),
        borderRadius : 10,
        flexDirection : 'row',
        justifyContent : 'space-between',
        alignItems : 'center',
        borderColor : '#0077BE',
        borderWidth : 1,
        padding : 15,
        marginBottom : hp('3.7%'),
    },
    txt_btn_akun:{
        fontFamily : 'Roboto-Medium',
        fontSize : normalize(14),
    },  
    tanggal_ulas :{
        fontFamily : 'Roboto-Regular',
        fontSize : normalize(13),
        textAlign : 'right',
    },
    ulasan:{
        fontFamily : 'Roboto-Regular',
        fontSize : normalize(13),
    },
    container_card_ulas:{
        padding : 10,
        borderWidth : 1,
        borderRadius : 10,
        borderColor : '#C4C4C4',
        marginBottom : hp('2.9%'),
    },
    
    
})

export default styles;