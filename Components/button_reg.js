import React, { Component } from 'react';
import { View, Text } from 'react-native';
import styles from './style'

export default class Button extends Component{
    render(props){
        return(
            <View style={this.props.style_button}>
                <Text style={this.props.style_txtButton}>{this.props.name}</Text>
            </View>
        )
    }
}
