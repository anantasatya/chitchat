import React, { useEffect, useState } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../screens/login/login';
import App from '../screens/chat/index';
import Signup from '../screens/signup/signup';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import auth from '@react-native-firebase/auth';
import Chat from '../screens/chat/chat';

const Stack = createStackNavigator();


function Aplikasi() {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  
  useEffect(() => {
    auth().onAuthStateChanged( async user => {
      setUser(user);
      if (initializing) setInitializing(false);
    });


  }, []);

  if (initializing) return null;

  if (!user) {
    return (
        <NavigationContainer>
          <Stack.Navigator 
              initialRouteName="Signup"
              screenOptions={{
                  headerStyle: {
                    backgroundColor: '#e9c46a',
                  },
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: '400',
                    textAlign : "center",
                    marginRight : wp('10%'),
                  },
                }}>
            <Stack.Screen 
              name="Login" 
              component={Login}
              options={{
                  headerShown : false
              }} />

              <Stack.Screen 
              name="Signup" 
              component={Signup}
              options={{
                  headerShown : false
              }} />
            <Stack.Screen 
              name="App" 
              component={App} 
              options={{
                  headerShown : false
              }}/>
          </Stack.Navigator>
        </NavigationContainer>
    
    );
  } else if(user){
    return(
      <NavigationContainer>
          <Stack.Navigator 
              initialRouteName="App"
              screenOptions={{
                  headerStyle: {
                    backgroundColor: '#0077BE',
                  },
                  headerTintColor: '#fff',
                  headerTitleStyle: {
                    fontWeight: '400',
                    textAlign : "center",
                    marginRight : wp('10%'),
                  },
                }}>
            <Stack.Screen 
              name="Login" 
              component={Login}
              options={{
                  headerShown : false
              }} />
              <Stack.Screen 
              name="Signup" 
              component={Signup}
              options={{
                  headerShown : false
              }} />
            <Stack.Screen 
              name="App" 
              component={App} 
              options={{
                  headerShown : false
              }}/>
            
          </Stack.Navigator>
        </NavigationContainer>
    );
    
  } 
}
  
  export default Aplikasi;